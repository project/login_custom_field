<?php

/**
 * @file
 * Helper functions for Login Custom Field module.
 */

/**
 * Functions for Login Custom Field module.
 */
function lcf_get_user_object_fields() {

  $fields_result = array();
  $allowed_field_types = array('number', 'phone_textfield', 'text_textfield');

  // List all user fields.
  $fields = field_info_instances('user');
  foreach ($fields['user'] as $field_key => $field) {
    if (in_array($field['widget']['type'], $allowed_field_types)) {
      $fields_result[$field_key] = $field['label'];
    }
  }

  return $fields_result;
}
