<?php

/**
 * @file
 * Included functions for Login Custom Field module.
 */

/**
 * Login Validation Using Selected Custom Field.
 */
function lcf_user_login_validate($form, &$form_state) {

  // @TODO: Support full mobile number as it is saved in database when field type is phone field.
  if (isset($form_state['values']['name']) && $form_state['values']['name']) {
    $query = db_select('field_data_' . variable_get('login_custom_field_by_field'), 'c')
                ->fields('u', array('name'))
                ->condition('c.' . variable_get('login_custom_field_by_field') . '_value', $form_state['values']['name'], '=')
                ->condition('c.entity_type', 'user', '=')
                ->groupBy('c.entity_id');
    $query->innerJoin('users', 'u', 'c.entity_id = u.uid');
    if ($name = $query->execute()->fetchField()) {
      form_set_value($form['name'], $name, $form_state);
    }
  }

}
