<?php

/**
 * @file
 * Administration functions for Login Custom Field module.
 */

/**
 * Main settings page.
 */
function lcf_main_settings() {
  $user_object_fields = lcf_get_user_object_fields();
  if (empty($user_object_fields)) {
    drupal_set_message(t('Please make sure you already have custom fields in your user object. !click_here', array('!click_here' => l(t('Manage account fields'), 'admin/config/people/accounts/fields'))), 'warning');
  }

  $form['login'] = array(
    '#type' => 'fieldset',
    '#title' => t('Log in'),
  );

  $form['login']['login_custom_field_status'] = array(
    '#type' => 'radios',
    '#title' => t('Enable custom field login functionality'),
    '#description' => t('In some cases you should clear cache since the login form cached for visitors.'),
    '#default_value' => variable_get('login_custom_field_status', 0),
    '#options' => array(0 => t('Disabled'), 1 => t('Enabled')),
    '#required' => TRUE,
  );

  $form['login']['login_custom_field_by_field'] = array(
    '#type' => 'select',
    '#title' => t('Allow login using this field'),
    '#description' => t('You should make sure the selected field is unique.') . '<br />' . t('<strong>Note:</strong> Allowed field types is (@integer, @text, @phone).', array(
      '@integer' => t('Integer'),
      '@text' => t('Text'),
      '@phone' => t('Phone Number'),
    )
    ),
    '#options' => $user_object_fields,
    '#default_value' => variable_get('login_custom_field_by_field'),
  );

  $form['#validate'][] = 'lcf_main_settings_validate';
  return system_settings_form($form);
}

/**
 * Validate settings form before save it.
 */
function lcf_main_settings_validate(&$form, &$form_state) {
  if ($form_state['values']['login_custom_field_by_field'] == '') {
    form_set_error('login_custom_field_by_field', t('Please make sure you already have custom fields in your user object. !click_here', array('!click_here' => l(t('Manage account fields'), 'admin/config/people/accounts/fields'))));
  }
}
